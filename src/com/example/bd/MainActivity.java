package com.example.bd;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	public Productos pt = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		this.deleteDatabase("Ventas.db");	//Elimino la base de datos si existe?
		//Creamos la base de datos:
		try {
			pt = new Productos(MainActivity.this);
			pt.apertura();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void inicializarBD(View v) throws Exception {
		try {
			pt.eliminarTODO();			
			Toast.makeText(this, "BD inicializado", 1).show();
		} catch (Exception e) {
			Toast.makeText(this, e.getMessage(), 1).show();
		}

	}
	public void mostrar(View v) throws Exception {
		TextView ptxt = (TextView) findViewById(R.id.textView1);
		try {
			String r = pt.Listar();
			ptxt.setText(r);
		} catch (Exception e) {
			Toast.makeText(this, e.getMessage(), 1).show();
		}
	}

	public void insertar(View vista) {
		Toast.makeText(this, "Llenando...", Toast.LENGTH_LONG).show();
		String estado = Environment.getExternalStorageState();
		if (!estado.equals(Environment.MEDIA_MOUNTED)) {
			Toast.makeText(this, "NO HAY SD CARD...", Toast.LENGTH_LONG).show();
			finish();
		}
		try {
			File dir = Environment.getExternalStorageDirectory();
			File pt2 = new File(dir.getAbsolutePath() + File.separator
					+ "productos.csv");
			BufferedReader lee = new BufferedReader(new FileReader(pt2));
			String linea = "";
			while ((linea = lee.readLine()) != null) {
				String datos[] = linea.split(";");
				long c = pt.Insertar(datos[0], datos[1], datos[2],
						Double.parseDouble(datos[3]), datos[5], datos[4]);
			}
			Toast.makeText(this, "Completado...", Toast.LENGTH_LONG).show();

		} catch (Exception e) {
			Toast.makeText(this, "Algo sali mal :0...", Toast.LENGTH_LONG)
					.show();
		}
	}

	public void formulario(View v) {
		Intent vd = new Intent(this, Formulario.class);
		startActivity(vd);
	}
	public void formularioBD(View v) {
		Intent vd = new Intent(this, FormularioBD.class);
		startActivity(vd);
	}
	public void finalizar(View vista) {
		finish();// Esta instruccion finaliza el hilo
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
