package com.example.bd;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class FormularioBD extends Activity {
	public Productos pt = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_formulario_bd);
		// Creamos la base de datos:
		try {
			pt = new Productos(FormularioBD.this);
			pt.apertura();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void buscar(View v) {
		String id = ((EditText) findViewById(R.id.EditText1)).getText()
				.toString();
		Toast.makeText(this, "id = " + id, Toast.LENGTH_LONG).show();
		String datos[] = pt.buscarPorId(id);
		// Declaramos variables:
		Log.d("datos", "res: "+datos[0]);
		EditText campo2 = (EditText) findViewById(R.id.EditText2);
		EditText campo3 = (EditText) findViewById(R.id.EditText3);
		EditText campo4 = (EditText) findViewById(R.id.EditText4);
		EditText campo5 = (EditText) findViewById(R.id.EditText5);
		EditText campo6 = (EditText) findViewById(R.id.EditText6);
		EditText campo7 = (EditText) findViewById(R.id.EditText7);
		EditText campo8 = (EditText) findViewById(R.id.EditText8);
		campo2.setText(datos[1]);
		campo3.setText(datos[2]);
		campo4.setText(datos[3]);
		campo5.setText(datos[5]);
		campo6.setText(datos[4]);
		campo7.setText(datos[2]);
		campo8.setText(datos[6]);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.formulario_bd, menu);
		return true;
	}

}
