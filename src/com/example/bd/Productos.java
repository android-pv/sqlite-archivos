package com.example.bd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class Productos {
	public static final String COD = "codigo";
	public static final String DES = "descripcion";
	public static final String UNV = "univen";
	public static final String PRU = "preciou";
	public static final String EXT = "existencia";
	public static final String LIN = "linea";
	private static final String NBD = "Ventas.db";
	private static final String NTB = "productos";
	private static final int VERSION = 1;
	private Creactua Control;
	private final Context nContexto;
	private SQLiteDatabase pBD;

	private static class Creactua extends SQLiteOpenHelper {
		public Creactua(Context context) {
			super(context, NBD, null, VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL("CREATE TABLE " + NTB + " (" + COD
					+ " INTEGER PRIMARY KEY," + DES + " TEXT NOT NULL, " + UNV
					+ " TEXT NOT NULL, " + PRU + " DECIMAL(6,2) NOT NULL,"
					+ EXT + " TEXT NOT NULL," + LIN + " TEXT NOT NULL);");
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int a, int n) {
			db.execSQL("DROP TABLE IF EXISTS " + NTB);
			onCreate(db);
		}
	}

	public Productos(Context c) {
		nContexto = c;
	}

	public Productos apertura() throws Exception {
		Control = new Creactua(nContexto);
		pBD = Control.getWritableDatabase();
		return this;
	}

	public void cerrar() {
		Control.close();
	}

	public long Insertar(String qCod, String qDes, String qUnv, Double qPrv,
			String qExt, String qLin) {
		ContentValues reg = new ContentValues();
		reg.put(COD, qCod);
		reg.put(DES, qDes);
		reg.put(UNV, qUnv);
		reg.put(PRU, qPrv);
		reg.put(EXT, qExt);
		reg.put(LIN, qLin);
		return pBD.insert(NTB, null, reg);
	}

	public String Listar() {
		String[] columnas = new String[] { COD, DES, PRU, EXT, LIN };
		Cursor c = pBD.query(NTB, columnas, null, null, null, null, null);
		String res = "";
		int iCod = c.getColumnIndex(COD);
		int iDes = c.getColumnIndex(DES);
		int iPru = c.getColumnIndex(PRU);
		int iExt = c.getColumnIndex(EXT);
		int iLin = c.getColumnIndex(LIN);
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			res = res + c.getString(iCod) + "  " + c.getString(iDes) + "  "
					+ c.getString(iPru) + "  " + c.getString(iExt) + " "
					+ c.getString(iLin) + "\n";
		}
		return res;
	}

	// Creamos estos metodos para eliminar o limpiar la base de datos.
	public boolean eliminaUno(String name) {
		return pBD.delete(NTB, COD + "=" + name, null) > 0;
	}

	public void eliminarTODO() {
		String[] columnas = new String[] { COD };
		Cursor c = pBD.query(NTB, columnas, null, null, null, null, null);
		int iCod = c.getColumnIndex(COD);
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			eliminaUno(c.getString(iCod));
		}
	}

	// Buscar por id
	public String[] buscarPorId(String id) {
		String respuesta[] = new String[7];// La ult. posicion guardara el
											// tiempo que tarda en ejecutar l
											// funcion
		long inicio = System.currentTimeMillis();
		String[] columnas = new String[] { COD, DES, UNV, PRU, EXT, LIN };
		Cursor c = pBD.query(NTB, columnas, null, null, null, null, null);
		int iCod = c.getColumnIndex(COD);
		int iDes = c.getColumnIndex(DES);
		int iUnv = c.getColumnIndex(UNV);
		int iPru = c.getColumnIndex(PRU);
		int iExt = c.getColumnIndex(EXT);
		int iLin = c.getColumnIndex(LIN);
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			Log.d("SQLite: ", c.getString(iCod));
			if (id.equals(c.getString(iCod))) {
				respuesta[0] = c.getString(iCod);
				respuesta[1] = c.getString(iDes);
				respuesta[2] = c.getString(iUnv);
				respuesta[3] = c.getString(iPru);
				respuesta[4] = c.getString(iExt);
				respuesta[5] = c.getString(iLin);
				long fin = System.currentTimeMillis();
				long tiempo = fin - inicio;
				respuesta[6] = tiempo + "milis";
				return respuesta;
			}
		}
		long fin = System.currentTimeMillis();
		long tiempo = fin - inicio;
		respuesta[6] = tiempo + "";
		return respuesta;
	}
}