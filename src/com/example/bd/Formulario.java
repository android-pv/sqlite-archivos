package com.example.bd;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Formulario extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_formulario);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.formulario, menu);
		return true;
	}

	public void buscar(View v) {
		// Obtengo el codigo
		EditText campoCodigo = (EditText) findViewById(R.id.EditText1);
		String codigo = campoCodigo.getText().toString();
		// Busco en los archivos
		buscarDatos(codigo);

	}

	public void buscarDatos(String codigo) {
		long inicio = System.currentTimeMillis();
		// Declaramos variables:
		EditText campo2 = (EditText) findViewById(R.id.EditText2);
		EditText campo3 = (EditText) findViewById(R.id.EditText3);
		EditText campo4 = (EditText) findViewById(R.id.EditText4);
		EditText campo5 = (EditText) findViewById(R.id.EditText5);
		EditText campo6 = (EditText) findViewById(R.id.EditText6);
		EditText campo7 = (EditText) findViewById(R.id.EditText7);
		EditText campo8 = (EditText) findViewById(R.id.EditText8);
		String estado = Environment.getExternalStorageState();
		if (!estado.equals(Environment.MEDIA_MOUNTED)) {
			Toast.makeText(this, "NO HAY SD CARD...", Toast.LENGTH_LONG).show();
			finish();
		}
		try {
			File dir = Environment.getExternalStorageDirectory();
			File pt2 = new File(dir.getAbsolutePath() + File.separator
					+ "productos.csv");
			BufferedReader lee = new BufferedReader(new FileReader(pt2));
			String linea = "";
			while ((linea = lee.readLine()) != null) {
				String datos[] = linea.split(";");
				if (codigo.equals(datos[0])) {

					campo2.setText(datos[1]);
					campo3.setText(datos[2]);
					campo4.setText(datos[3]);
					campo5.setText(datos[0]);
					campo6.setText(datos[5]);
					campo7.setText(datos[3]);
					long fin = System.currentTimeMillis();
					long res = fin - inicio;
					campo8.setText(res + " milis");
					return;
				}

			}

		} catch (Exception e) {
			Toast.makeText(this, "Algo sali mal :0...", Toast.LENGTH_LONG)
					.show();
		}
	}
}
