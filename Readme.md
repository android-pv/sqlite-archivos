# Objetivo:

Manipular la BD y archivos con android

# Pre instalacion:

Subir a la memoria de la maquina virtual de android el archivo 'productos.csv' para poder leerlo en el programa (Esta en la raiz del proyecto)


# Instalacion:

PASO 1:
Descargar de cualquiera de estos modos:
- Descargar el proyecto en .zip
- Clonar el repositorio con: `git clone https://gitlab.com/android-pv/sqlite-archivos.git`

PASO 2:
Entrar al ECLIPSE ANDROID (o android studio)

PASO 3:
Importar el proyecto con Import > Android > Existing Android Code Into Workspace

PASO 4:
Seleccionar el archivo donde descargamos en el PASO 1 (Tickear el 'Copy projects into workspace') y 'Add project to working sets'

PASO 5:
Ejecutar el proyecto :D